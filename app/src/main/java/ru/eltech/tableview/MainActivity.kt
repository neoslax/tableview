package ru.eltech.tableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import ru.eltech.tableview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val images = listOf<Int>(
            R.drawable.img1,
            R.drawable.img2,
            R.drawable.img3,
            R.drawable.img4,
            R.drawable.img5,
            R.drawable.img6
        )


        binding.button.setOnClickListener {
            binding.imageView.background = AppCompatResources.getDrawable(
                this,
                getRandomPic(images)
            )
            binding.button.setBackgroundColor(getRandomColor())

        }

    }

    private fun getRandomPic(img: List<Int>): Int {
        val size = img.size
        val picNum = (Math.random() * size).toInt()
        return img[picNum]
    }

    private fun getRandomColor(): Int {
        val r = (Math.random() * 255).toInt()
        val g = (Math.random() * 255).toInt()
        val b = (Math.random() * 255).toInt()
        return Color.rgb(r, g, b)
    }
}